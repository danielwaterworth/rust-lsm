# rust-lsm

This is an implementation of a log-structured merge tree in
Rust, designed for clarity over efficiency:

I recommend reading through the code in dependency order:

 * `format.rs` contains logic for (de)serialization of sorted string
   tables,
 * `index.rs` provides views of a particular `SSTable`, currently
   storing all of the keys in a `HashMap`,
 * `store.rs` contains the LSM logic,

The code for merging files has known bugs:

 * It doesn't handle errors correctly,
 * It assumes that merging produces sufficiently large files to be added
   to the next level,

There are other shortcomings:

 * The DB state isn't read from disk on initialization,
 * There is no way to issue deletes, although tombstones are handled
   correctly on the query side,
