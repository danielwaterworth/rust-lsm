use std::io::{Read, Result, Seek};
use std::collections::HashMap;
use std::sync::Mutex;

use crate::format::{SSTableReader, Value};

pub struct Indexed<R: Read + Seek> {
    entries: HashMap<String, u64>,
    reader: SSTableReader<R>,
}

impl<R: Read + Seek> Indexed<R> {
    pub fn new(mut reader: SSTableReader<R>) -> Result<Self> {
        reader.seek(0)?;

        let mut entries = HashMap::new();

        loop {
            let offset = reader.offset()?;

            match reader.read_entry() {
                Ok(Some((key, _value))) => {
                    entries.insert(key, offset);
                },
                Ok(None) => {
                    break;
                },
                Err(err) => {
                    return Err(err);
                }
            }
        }

        Ok(
            Indexed {
                entries,
                reader,
            }
        )
    }

    // The return type here is a little ridiculous
    //
    // * An IO operation could fail,
    // * The value may not be present in the index,
    // * The value may be a tombstone,
    pub fn get(&mut self, key: &str) -> Result<Option<Value<String>>> {
        let offset =
            match self.entries.get(key) {
                None => {
                    return Ok(None)
                },
                Some(offset) => offset
            };

        self.reader.seek(*offset)?;
        let (_key, value) =
            self.reader
                .read_entry()?
                .expect("We stored an offset where there was no entry");
        Ok(Some(value))
    }
}

pub struct LockedIndexed<R: Read + Seek> {
    indexed: Mutex<Indexed<R>>,
}

impl<R: Read + Seek> LockedIndexed<R> {
    pub fn new(reader: SSTableReader<R>) -> Result<Self> {
        Indexed::new(reader)
            .map(|indexed|
                LockedIndexed {
                    indexed: Mutex::new(indexed)
                }
            )
    }

    pub fn get(&self, key: &str) -> Result<Option<Value<String>>> {
        self.indexed.lock().unwrap().get(key)
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Cursor, Seek, SeekFrom};
    use super::*;
    use crate::format::*;

    #[test]
    fn write_and_read() {
        let mut pipe = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
            writer.write_entry("goo", Value::Exists("baz")).unwrap();
        }

        pipe.seek(SeekFrom::Start(0)).unwrap();

        let reader = SSTableReader::new(&mut pipe);
        let mut indexed = Indexed::new(reader).unwrap();

        assert_eq!(
            indexed.get("foo").unwrap(),
            Some(Value::Exists("bar".into())),
        );

        assert_eq!(
            indexed.get("goo").unwrap(),
            Some(Value::Exists("baz".into())),
        );

        assert!(
            indexed.get("bob").unwrap().is_none(),
        );
    }

    #[test]
    fn write_and_read_locked() {
        let mut pipe = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
            writer.write_entry("goo", Value::Exists("baz")).unwrap();
        }

        pipe.seek(SeekFrom::Start(0)).unwrap();

        let reader = SSTableReader::new(&mut pipe);
        let indexed = LockedIndexed::new(reader).unwrap();

        assert_eq!(
            indexed.get("foo").unwrap(),
            Some(Value::Exists("bar".into())),
        );

        assert_eq!(
            indexed.get("goo").unwrap(),
            Some(Value::Exists("baz".into())),
        );

        assert!(
            indexed.get("bob").unwrap().is_none(),
        );
    }
}
