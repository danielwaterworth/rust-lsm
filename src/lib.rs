#![feature(integer_atomics)]
#![feature(arbitrary_self_types)]

mod format;
mod index;
pub mod store;
