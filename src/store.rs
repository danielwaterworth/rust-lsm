use std::fs::{File};
use std::fs;
use std::io::{Result};
use std::mem;
use std::path::{Path, PathBuf};
use std::ptr;
use std::sync::Condvar as CondVar;
use std::sync::atomic::{AtomicU64, Ordering};
use std::sync::{Arc, Mutex, MutexGuard};
use std::thread;

use crate::format::*;
use crate::index::LockedIndexed;

const SLOTS: usize = 9;
const MAJORITY: usize = 5;

#[derive(Clone)]
struct Level {
    indices: [Option<Arc<(PathBuf, LockedIndexed<File>, u64)>>; SLOTS],
    has_space: Arc<CondVar>,
}

enum PushResult {
    Success(u8),
    Fail(LockedIndexed<File>),
}

impl Level {
    fn empty() -> Self {
        let mut indices:
                [Option<Arc<(PathBuf, LockedIndexed<File>, u64)>>; SLOTS] =
            unsafe {
                mem::uninitialized()
            };

        for i in 0..indices.len() {
            unsafe {
                ptr::write(&mut indices[i], None);
            }
        }

        Level {
            indices,
            has_space: Arc::new(CondVar::new()),
        }
    }

    fn squish(&mut self) -> u8 {
        // Moving from the back to the front, move the entries towards
        // the end, preserving their order.
        let mut beyond_store_index = self.indices.len();
        let mut num_tables = 1;
        for i in (0..self.indices.len()).rev() {
            let store_index = beyond_store_index - 1;
            if self.indices[i].is_some() {
                num_tables += 1;
                if i < store_index {
                    let (before, after) =
                        self.indices.split_at_mut(store_index);
                    mem::swap(
                        &mut before[i],
                        &mut after[0],
                    );
                }
                beyond_store_index -= 1;
            }
        }
        num_tables
    }

    fn push(
                &mut self,
                new_filename: PathBuf,
                index: LockedIndexed<File>,
                n: u64,
            ) -> PushResult {
        let num_tables = self.squish();

        // If the first spot is now open, we can store the new index
        if self.indices[0].is_none() {
            self.indices[0] = Some(Arc::new((new_filename, index, n)));
            PushResult::Success(num_tables)
        } else {
            PushResult::Fail(index)
        }
    }
}

pub struct Store {
    tmp_counter: AtomicU64,
    levels: Mutex<Vec<Level>>,
    dir: PathBuf,
}

impl Store {
    pub fn new(dir: &Path) -> Arc<Self> {
        Arc::new(
            Store {
                tmp_counter: AtomicU64::new(0),
                dir: dir.to_owned(),
                levels: Mutex::new(Vec::new()),
            }
        )
    }

    pub fn insert(self: &Arc<Self>, entries: &[(&str, &str)]) -> Result<()> {
        let (file, filename, n) = self.new_tmp_file()?;
        let mut writer = SSTableWriter::new(file);
        for (key, value) in entries {
            writer.write_entry(key, Value::Exists(value))?;
        }
        self.cascade_add(0, filename, n)
    }

    pub fn get(self: &Arc<Self>, key: &str) -> Result<Option<String>> {
        // Clone here so that we don't hold the lock whilst doing I/O
        let levels = self.levels.lock().unwrap().clone();

        for level in levels.iter() {
            for index in level.indices.iter() {
                match index.as_ref() {
                    None => {},
                    Some(index) =>
                        match index.1.get(key)? {
                            Some(Value::Exists(value)) =>
                                return Ok(Some(value)),
                            Some(Value::Tombstone) =>
                                return Ok(None),
                            None => {},
                        },
                }
            }
        }
        Ok(None)
    }

    fn new_tmp_file(self: &Arc<Self>) -> Result<(File, PathBuf, u64)> {
        let n = self.tmp_counter.fetch_add(1, Ordering::Relaxed);
        let filename = format!("tmp_{}.sstable", n);
        let path = self.dir.join(filename);
        Ok((File::create(&path)?, path, n))
    }

    fn trigger_merge(self: &Arc<Self>, level: usize) {
        // FIXME: Do something with the thread handle
        // FIXME: Decide where errors in merge operations go
        //
        let this = self.clone();
        thread::spawn(move || {
            let (file, filename, n) = this.new_tmp_file().unwrap();

            // This block exists to force the mutex to be unlocked before
            // cascading
            {
                let level_ref = &mut this.levels.lock().unwrap()[level];

                let num_tables = level_ref.squish();
                assert!(num_tables as usize >= MAJORITY);

                let level_obj = level_ref.clone();

                let mut readers = Vec::new();
                for i in SLOTS-MAJORITY..SLOTS {
                    readers.push(
                        PeekableSSTableReader::new(
                            SSTableReader::new(
                                File::open(
                                    &level_obj.indices[i].as_ref().unwrap().0
                                ).unwrap(),
                            )
                        ).unwrap()
                    );
                }

                let mut merger = MergeSSTableReader::new(&mut readers[..]);
                let mut writer = SSTableWriter::new(file);

                let mut last_key: Option<String> = None;
                loop {
                    match merger.read_entry().unwrap() {
                        None => {
                            break;
                        },
                        Some((key, value)) => {
                            if Some(&key) != last_key.as_ref() {
                                last_key = Some(key.clone());

                                match value {
                                    Value::Tombstone =>
                                        writer.write_entry(
                                            &key,
                                            Value::Tombstone,
                                        ),
                                    Value::Exists(x) =>
                                        writer.write_entry(
                                            &key,
                                            Value::Exists(x.as_str()),
                                        ),
                                }.unwrap();
                            }
                        },
                    }
                }
            }

            this.cascade_add(level + 1, filename, n).unwrap();

            let level_ref = &mut this.levels.lock().unwrap()[level];

            for i in SLOTS-MAJORITY..SLOTS {
                let filename = &level_ref.indices[i].as_ref().unwrap().0;
                fs::remove_file(filename).unwrap();
                level_ref.indices[i] = None;
            }

            level_ref.has_space.notify_all();
        });
    }

    fn cascade_add(self: &Arc<Self>, level: usize, filename: PathBuf, n: u64)
            -> Result<()> {
        let new_filename = self.dir.join(format!("{}_{}.sstable", level, n));
        let file = File::open(&filename)?;
        let mut index =
            LockedIndexed::new(
                SSTableReader::new(file)
            )?;

        let mut levels_guard: MutexGuard<Vec<Level>> =
            self.levels.lock().unwrap();

        let levels_ref: &mut Vec<Level> = &mut levels_guard;

        if level >= levels_ref.len() {
            for _ in levels_ref.len()..(level + 1) {
                levels_ref.push(Level::empty());
            }
        }

        loop {
            let levels_ref: &mut Vec<Level> =
                &mut levels_guard;

            let level_ref: &mut Level = &mut levels_ref[level];
            let has_space = level_ref.has_space.clone();

            match level_ref.push(new_filename.clone(), index, n) {
                PushResult::Success(num_tables) => {
                    fs::rename(
                        &filename,
                        &new_filename,
                    )?;
                    if num_tables as usize == MAJORITY {
                        self.trigger_merge(level);
                    }
                    break;
                },
                PushResult::Fail(x) => {
                    index = x;
                    levels_guard = has_space.wait(levels_guard).unwrap();
                },
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn simple_store() {
        let tmp_dir = PathBuf::from("/tmp/db.1/");
        fs::create_dir(&tmp_dir);
        let store = Store::new(tmp_dir.as_ref());
        store.insert(&[
            ("foo", "bar"),
            ("goo", "baz"),
        ]).unwrap();

        assert_eq!(
            store.get("foo").unwrap(),
            Some("bar".into()),
        );

        assert_eq!(
            store.get("bob").unwrap(),
            None,
        );
    }

    #[test]
    fn store_twice() {
        let tmp_dir = PathBuf::from("/tmp/db.2/");
        fs::create_dir(&tmp_dir);
        let store = Store::new(tmp_dir.as_ref());

        store.insert(&[
            ("foo", "bar1"),
            ("goo", "baz1"),
        ]).unwrap();

        store.insert(&[
            ("foo", "bar2"),
            ("hoo", "baz2"),
        ]).unwrap();

        assert_eq!(
            store.get("foo").unwrap(),
            Some("bar2".into()),
        );

        assert_eq!(
            store.get("goo").unwrap(),
            Some("baz1".into()),
        );

        assert_eq!(
            store.get("hoo").unwrap(),
            Some("baz2".into()),
        );

        assert_eq!(
            store.get("bob").unwrap(),
            None,
        );
    }

    #[test]
    fn fill_first_level() {
        let tmp_dir = PathBuf::from("/tmp/db.3/");
        fs::create_dir(&tmp_dir);
        let store = Store::new(tmp_dir.as_ref());

        for i in 0..9 {
            store.insert(&[
                (&format!("key_{}", i), &format!("value_{}", i)),
            ]).unwrap();
        }
    }

    #[test]
    fn write_to_second_level() {
        let tmp_dir = PathBuf::from("/tmp/db.4/");
        fs::create_dir(&tmp_dir);
        let store = Store::new(tmp_dir.as_ref());

        for i in 0..10 {
            store.insert(&[
                (&format!("key_{}", i), &format!("value_{}", i)),
            ]).unwrap();
        }
    }
}
