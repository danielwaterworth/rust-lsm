use std::io::{Read, Result, Write, Seek, SeekFrom, Error, ErrorKind};
use std::mem;
use std::cmp::Ordering;

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};

#[derive(Debug, PartialEq)]
pub enum Value<V> {
    Exists(V),
    Tombstone,
}

pub struct SSTableWriter<W: Write> {
    writer: W,
}

impl<W: Write> SSTableWriter<W> {
    pub fn new(writer: W) -> Self {
        SSTableWriter {
            writer
        }
    }

    fn write_string(&mut self, value: &str) -> Result<()> {
        let value_bytes = value.as_bytes();
        let length = value_bytes.len();
        let length_u32 = length as u32;

        if length_u32 as usize != length {
            panic!("Value too large");
        }

        self.writer.write_u32::<LittleEndian>(length_u32)?;
        self.writer.write_all(value_bytes)
    }

    pub fn write_entry(&mut self, key: &str, value: Value<&str>)
            -> Result<()> {
        self.write_string(key)?;
        match value {
            Value::Exists(value) => {
                self.writer.write_all(&[0])?;
                self.write_string(value)
            },
            Value::Tombstone => self.writer.write_all(&[1])
        }
    }
}

pub struct SSTableReader<R: Read + Seek> {
    reader: R,
}

impl<R: Read + Seek> SSTableReader<R> {
    pub fn new(reader: R) -> Self {
        SSTableReader {
            reader
        }
    }

    fn read_string(&mut self) -> Result<String> {
        let length_bytes = self.reader.read_u32::<LittleEndian>()?;
        let mut bytes = vec![0; length_bytes as usize];
        self.reader.read_exact(bytes.as_mut_slice())?;
        String::from_utf8(bytes).map_err(|_| {
            Error::new(
                ErrorKind::Other,
                "Expected UTF-8 encoded string",
            )
        })
    }

    pub fn read_entry(&mut self) -> Result<Option<(String, Value<String>)>> {
        match self.read_string() {
            Ok(key) => {
                let mut tombstone = [0];
                self.reader.read_exact(&mut tombstone[..])?;
                let value =
                    if tombstone[0] == 1 {
                        Value::Tombstone
                    } else {
                        Value::Exists(self.read_string()?)
                    };
                Ok(Some((key, value)))
            },
            Err(ref err) if err.kind() == ErrorKind::UnexpectedEof => {
                Ok(None)
            },
            Err(err) => {
                Err(err)
            },
        }
    }

    pub fn offset(&mut self) -> Result<u64> {
        self.reader.seek(SeekFrom::Current(0))
    }

    pub fn seek(&mut self, offset: u64) -> Result<()> {
        self.reader.seek(SeekFrom::Start(offset)).map(|_| ())
    }

    pub fn peekable(self) -> Result<PeekableSSTableReader<R>> {
        PeekableSSTableReader::new(self)
    }
}

pub struct PeekableSSTableReader<R: Read + Seek> {
    reader: SSTableReader<R>,
    offset: u64,
    value: Result<Option<(String, Value<String>)>>,
}

impl<R: Read + Seek> PeekableSSTableReader<R> {
    pub fn new(mut reader: SSTableReader<R>) -> Result<Self> {
        let offset = reader.offset()?;
        let value = reader.read_entry();

        Ok(
            PeekableSSTableReader {
                reader,
                offset,
                value,
            }
        )
    }

    pub fn peek(&self) -> &Result<Option<(String, Value<String>)>> {
        &self.value
    }

    pub fn offset(&self) -> u64 {
        self.offset
    }

    pub fn advance(&mut self) -> Result<Option<(String, Value<String>)>> {
        self.offset = self.reader.offset()?;

        let mut tmp = self.reader.read_entry();
        mem::swap(&mut tmp, &mut self.value);
        tmp
    }
}

// For larger numbers of readers, it would be more efficient to form a
// tree of mergers, but this is not currently possible.
pub struct MergeSSTableReader<'a, R: Read + Seek> {
    readers: &'a mut [PeekableSSTableReader<R>],
}

impl<'a, R: Read + Seek> MergeSSTableReader<'a, R> {
    pub fn new(readers: &'a mut [PeekableSSTableReader<R>]) -> Self {
        MergeSSTableReader {
            readers
        }
    }

    pub fn read_entry(&mut self) -> Result<Option<(String, Value<String>)>> {
        // Err(_) means that one of the readers hit an unexpected problem.
        // this would be the most interesting thing and hence least in
        // the ordering.
        //
        // Ok(Some((k, v))) means that a reader found an entry, the smallest
        // entry is the next one to return so long as no reader hits an error.
        //
        // Ok(None) means that we came to the end of the file, this is only
        // relevant if all the readers have come to the end.
        //
        let min =
            self.readers.iter_mut().min_by(|x1, x2|
                match (x1.peek(), x2.peek()) {
                    (&Err(_), &Err(_)) => Ordering::Equal,
                    (&Err(_), _) => Ordering::Less,
                    (_, &Err(_)) => Ordering::Greater,
                    (&Ok(Some((ref k1, _))), &Ok(Some((ref k2, _)))) =>
                        k1.cmp(k2),
                    (&Ok(None), &Ok(None)) => Ordering::Equal,
                    (&Ok(None), _) => Ordering::Greater,
                    (_, &Ok(None)) => Ordering::Less,
                }
            ).ok_or_else(||
                Error::new(ErrorKind::Other, "no readers to draw input from")
            )?;
        min.advance()
    }
}

#[cfg(test)]
mod tests {
    use std::io::{Cursor, Seek, SeekFrom};
    use super::*;

    #[test]
    fn write_and_read() {
        let mut pipe = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
        }

        pipe.seek(SeekFrom::Start(0)).unwrap();

        {
            let mut reader = SSTableReader::new(&mut pipe);
            assert_eq!(
                reader.read_entry().unwrap().unwrap(),
                ("foo".into(), Value::Exists("bar".into()))
            );
        }
    }

    #[test]
    fn write_two_then_read_offset_read_seek_read() {
        let mut pipe = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
            writer.write_entry("goo", Value::Exists("baz")).unwrap();
        }

        pipe.seek(SeekFrom::Start(0)).unwrap();

        {
            let mut reader = SSTableReader::new(&mut pipe);
            assert_eq!(
                reader.read_entry().unwrap().unwrap(),
                ("foo".into(), Value::Exists("bar".into()))
            );

            assert_eq!(
                reader.offset().unwrap(),
                15,
            );

            assert_eq!(
                reader.read_entry().unwrap().unwrap(),
                ("goo".into(), Value::Exists("baz".into()))
            );

            reader.seek(15).unwrap();

            assert_eq!(
                reader.read_entry().unwrap().unwrap(),
                ("goo".into(), Value::Exists("baz".into()))
            );
        }
    }

    #[test]
    fn test_peekable() {
        let mut pipe = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
            writer.write_entry("goo", Value::Exists("baz")).unwrap();
        }

        pipe.seek(SeekFrom::Start(0)).unwrap();

        {
            let reader = SSTableReader::new(&mut pipe);
            let mut peekable = PeekableSSTableReader::new(reader).unwrap();

            assert_eq!(
                peekable.peek().as_ref().unwrap().as_ref().unwrap(),
                &("foo".into(), Value::Exists("bar".into())),
            );

            assert_eq!(
                peekable.offset(),
                0,
            );

            assert_eq!(
                peekable.advance().unwrap().unwrap(),
                ("foo".into(), Value::Exists("bar".into())),
            );

            assert_eq!(
                peekable.peek().as_ref().unwrap().as_ref().unwrap(),
                &("goo".into(), Value::Exists("baz".into())),
            );

            assert_eq!(
                peekable.offset(),
                15,
            );
        }
    }

    #[test]
    fn test_merge() {
        let mut pipe1 = Cursor::new(Vec::new());
        let mut pipe2 = Cursor::new(Vec::new());
        let mut pipe3 = Cursor::new(Vec::new());

        {
            let mut writer = SSTableWriter::new(&mut pipe1);
            writer.write_entry("foo", Value::Exists("bar")).unwrap();
        }

        {
            let mut writer = SSTableWriter::new(&mut pipe2);
            writer.write_entry("goo", Value::Exists("baz")).unwrap();
        }

        pipe1.seek(SeekFrom::Start(0)).unwrap();
        pipe2.seek(SeekFrom::Start(0)).unwrap();

        let mut peekables = [
            SSTableReader::new(&mut pipe1).peekable().unwrap(),
            SSTableReader::new(&mut pipe2).peekable().unwrap(),
            SSTableReader::new(&mut pipe3).peekable().unwrap(),
        ];

        let mut merged = MergeSSTableReader::new(&mut peekables);

        assert_eq!(
            merged.read_entry().unwrap().unwrap(),
            ("foo".into(), Value::Exists("bar".into())),
        );

        assert_eq!(
            merged.read_entry().unwrap().unwrap(),
            ("goo".into(), Value::Exists("baz".into())),
        );

        assert!(merged.read_entry().unwrap().is_none());
    }
}
